# CS 373: Software Engineering Collatz Repo

* Name: Daniel Gao

* EID: djg3296

* GitLab ID: 18dgao

* HackerRank ID: danielgao

* Git SHA: f8996ef952bde7d1c798361d5ea7c9f7d4035045 

* GitLab Pipelines: https://gitlab.com/18dgao/cs373-collatz/-/pipelines

* Estimated completion time: 10 hrs

* Actual completion time: 5 hrs

* Comments: Fun project
