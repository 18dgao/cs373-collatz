#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = global-statement
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

cache_warm = False  # whether caches have been preloaded or not
value_cache = {1: 1}  # Cache holding collatz cycle lengths for values
max_cache = [
    0
] * 1000001  # Cache which holds a running max of collatz cycle lengths up to index


def collatz_preprocess():
    """
        Method used to warm up collatz caches so that eval will be able to run quickly
    """
    global cache_warm
    cache_warm = True

    def collatz(x):
        """
            Submethod that computes the collatz cycle length of a given value
            and updates cache with computed value
        """
        assert x > 0
        val = value_cache.get(x)  # check to see if value is already cached
        if val is not None:
            return val
        nxt = 3 * x + 1 if x % 2 else x / 2  # run collatz iteration and recurse
        val = 1 + collatz(nxt)
        if x <= 1000000:
            value_cache[
                x
            ] = val  # Stores computed value in cache only if it is within 1-1000000
        return val

    # Loops from 1 to 1000000 and populates the max cache as well as value cache
    for i in range(1, 1000001):
        max_cache[i] = max(max_cache[i - 1], collatz(i))


# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a tuple of two ints
    """
    a = s.split()
    return int(a[0]), int(a[1])


# ------------
# collatz_eval
# ------------


def collatz_eval(t):
    """
    t a tuple of two ints
    return a tuple of three ints
    """
    if not cache_warm:
        collatz_preprocess()  # Eager cache results if not done yet
    assert cache_warm

    i, j = t

    # compute start and stop values
    start = min(i, j)
    stop = max(i, j)
    assert start <= stop
    max_val = 1

    # Compute optimal start, aka whichever is bigger between start and stop/2
    opt_start = max(int(stop / 2), start)

    if max_cache[stop] > max_cache[start]:
        # If max cycle length 1...stop is bigger than max cycle length 1...start then we know
        # max cycle length 1...stop == mcl start...stop
        max_val = max_cache[stop]
    else:
        # Otherwise, if max cache doesn't help then just manually look up the range
        for k in range(opt_start, stop + 1):
            max_val = max(value_cache[k], max_val)
    assert max_val > 0
    return i, j, max_val


# -------------
# collatz_print
# -------------


def collatz_print(sout, t):
    """
    print three ints
    sout a writer
    t a tuple of three ints
    """
    i, j, v = t
    sout.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(sin, sout):
    """
    sin  a reader
    sout a writer
    """
    for s in sin:
        collatz_print(sout, collatz_eval(collatz_read(s)))
